@extends('index')

@section('title', 'Авторизация')

@section('main')
    <div id="auth">
        <form method="POST" action="/auth/login">
            {!! csrf_field() !!}
            @if (count($errors) > 0)
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif
            <div>
                <label for="email">Email</label>
                <input class="form-control" type="email" name="email" value="{{ old('email') }}" title="email">
            </div>
            <div>
                <label for="password">Пароль</label>
                <input class="form-control" type="password" name="password" id="password">
            </div>
            <div>
                <label style="text-align: right; width: 100%;">
                    <input type="checkbox" name="remember" title="Повторить">
                    Запомнить меня
                </label>
            </div>
            <div>
                <button type="submit" class="btn btn-primary btn-lg btn-block">Войти</button>
            </div>
        </form>
    </div>
@stop