@extends('index')

@section('title', 'Авторизация')

@section('main')
    <div id="auth">
        <form method="POST" action="/auth/register">
            {!! csrf_field() !!}
            @if (count($errors) > 0)
                <ul class="error-message">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif
            <div>
                <label for="name">Псевдоним</label>
                <input class="form-control" type="text" name="name" value="{{ old('name') }}" title="Псевдоним">
            </div>

            <div>
                <label for="email">Email</label>
                <input class="form-control" type="email" name="email" value="{{ old('email') }}" title="email">
            </div>

            <div>
                <label for="password">Пароль</label>
                <input class="form-control" type="password" name="password" title="Пароль">
            </div>

            <div>
                <label for="password_confirmation">Повторите пароль</label>
                <input class="form-control" type="password" name="password_confirmation" title="Пароль">
            </div>

            <div>
                <button type="submit" class="btn btn-primary btn-lg btn-block">Зарегистрироваться</button>
            </div>
        </form>
    </div>
@stop