<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="/assets/style/styles.css">
</head>
<body>
    <div id="home">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="/" class="car-ico navbar-brand"></a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li><a href="/">Главная</a></li>
                    </ul>
                    @if(!Auth::check())
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="/auth/login">Вход</a></li>
                        <li><a href="/auth/register">Регистрация</a></li>
                    </ul>
                    @else
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="/auth/logout">Выход</a></li>
                        </ul>
                    @endif
                </div>
            </div>
        </nav>
        <div id="wrapper" class="container">
            <div id="main">
                @yield('main')
            </div>
        </div>
        <div class="buffer"></div>
    </div>
    <div id="footer">
        <div class="container">
            <span class="course-name">Курсовой проект по теме  <span class="course-theme">"Экспертная система выбора автомобилей"</span></span>
            <span class="course-student">Студент: Немарский Евгений Владимирович</span>
        </div>
    </div>
    <script src="/assets/js/app.js"></script>
</body>
</html>