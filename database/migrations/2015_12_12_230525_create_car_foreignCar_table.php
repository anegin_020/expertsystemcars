<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarForeignCarTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('car_foreignCar', function (Blueprint $table) {
            $table->integer('car_id')->unsigned()->index();
            $table->foreign('car_id')->references('id')->on('car')->onDelete('cascade');
            $table->integer('foreignCar_id')->unsigned()->index();
            $table->foreign('foreignCar_id')->references('id')->on('foreignCar')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('car_foreignCar');
    }
}
