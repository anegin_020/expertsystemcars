<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDescriptionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('description', function(Blueprint $table){
            $table->string('wheels', 255)->nullable();
            $table->string('salon', 255)->nullable();
            $table->string('seatСovers', 255)->nullable();
            $table->string('weight', 255)->nullable();
            $table->string('bodyType', 255)->nullable();
            $table->string('technologies', 255)->nullable();
            $table->string('airCoolingSystem', 255)->nullable();
            $table->string('colors', 255)->nullable();
            $table->string('fuel', 255)->nullable();
            $table->string('engineCapacity', 255)->nullable();
            $table->integer('car_id')->unsigned()->index();
            $table->foreign('car_id')->references('id')->on('car')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('description');
    }
}
