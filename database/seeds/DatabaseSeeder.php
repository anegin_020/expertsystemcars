<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table('users')->insert([
            'name'=>'Anegin',
            'email'=>'anegin_020@mail.ru',
            'password'=>bcrypt('123456')
        ]);

        $this->call(QuestionSeeder::class);
        $this->call(AnswerSeeder::class);

        //Model::reguard();
    }
}
